package com.example.libarymanagement.dto;

import com.example.libarymanagement.model.Staff;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Data
public class ReportDTO {
    private int id;
    private String status;

    private Staff staff;
}
