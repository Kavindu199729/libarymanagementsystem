package com.example.libarymanagement.dto;

import lombok.*;

import java.time.Instant;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
public class UserDTO {
    private int id;
    private String name;
    private String password;

    private Instant createdAt;

    private Instant updatedAt;

}
