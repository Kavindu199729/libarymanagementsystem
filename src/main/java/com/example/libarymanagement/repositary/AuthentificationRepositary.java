package com.example.libarymanagement.repositary;

import com.example.libarymanagement.model.Authentication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthentificationRepositary extends JpaRepository<Authentication, Integer> {
}
