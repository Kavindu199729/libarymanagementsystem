package com.example.libarymanagement.repositary;

import com.example.libarymanagement.model.ReportModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepositary extends JpaRepository<ReportModel,Integer> {
}
