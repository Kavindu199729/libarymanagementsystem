package com.example.libarymanagement.repositary;

import com.example.libarymanagement.model.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaffRepositary extends JpaRepository<Staff,Integer> {
}
