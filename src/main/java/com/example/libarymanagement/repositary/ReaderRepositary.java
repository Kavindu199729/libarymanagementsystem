package com.example.libarymanagement.repositary;

import com.example.libarymanagement.model.Reader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReaderRepositary extends JpaRepository<Reader, Integer> {
    Page<Reader> findAll(Pageable pageable);

}
