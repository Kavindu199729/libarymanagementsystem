package com.example.libarymanagement.repositary;

import com.example.libarymanagement.model.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PublishRepositary extends JpaRepository<Publisher,Integer> {
}
