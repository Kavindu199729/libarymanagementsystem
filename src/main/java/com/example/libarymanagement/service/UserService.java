package com.example.libarymanagement.service;

import com.example.libarymanagement.dto.UserDTO;
import com.example.libarymanagement.model.User;

import java.util.List;

public interface UserService {
    User saveUser(UserDTO userDTO);

    void updateUser(int id, UserDTO userDTO);

    List<User> gettingUser();

    public void deleteUser(int id);
}
