package com.example.libarymanagement.service;


import com.example.libarymanagement.dto.ReportDTO;
import com.example.libarymanagement.model.ReportModel;

public interface ReportService {
    public ReportModel saveReport(ReportDTO reportDTO);
    public void reportDelete(int id);
}
