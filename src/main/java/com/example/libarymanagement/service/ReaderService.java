package com.example.libarymanagement.service;

import com.example.libarymanagement.model.Reader;
import org.springframework.data.domain.Page;


import java.util.List;


public interface ReaderService {
    public Reader saveReader(Reader reader);
     public void updateReader(int id,Reader reader);
    public List<Reader> readerList();
    public void deleteReader(int id);
    public Page<Reader> getAllReaders(int pageNumber, int pageSize, String sortBy);
}
