package com.example.libarymanagement.service.impl;

import com.example.libarymanagement.dto.ReportDTO;
import com.example.libarymanagement.model.ReportModel;
import com.example.libarymanagement.repositary.ReportRepositary;
import com.example.libarymanagement.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportServiceImpl implements ReportService {
    @Autowired
    public ReportRepositary reportRepositary;

    @Override
    public ReportModel saveReport(ReportDTO reportDTO) {
        ReportModel reportModel = new ReportModel();
        reportModel.setStatus(reportDTO.getStatus());
        reportModel.setStaff(reportDTO.getStaff());
        return reportRepositary.save(reportModel);
    }

    @Override
    public void reportDelete(int id) {
        reportRepositary.deleteById(id);
    }
}
