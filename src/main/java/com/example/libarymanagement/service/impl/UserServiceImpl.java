package com.example.libarymanagement.service.impl;

import com.example.libarymanagement.dto.UserDTO;
import com.example.libarymanagement.model.User;
import com.example.libarymanagement.repositary.UserRepositary;
import com.example.libarymanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepositary userRepositary;

    @Override
    public User saveUser(UserDTO userDTO) {
        User user = new User();
        user.setName(userDTO.getName());
        user.setPassword(userDTO.getPassword());
        return userRepositary.save(user);
    }

    @Override
    public void updateUser(int id, UserDTO userDTO) {
        Optional<User> optionalUser = userRepositary.findById(id);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setName(userDTO.getName());
            user.setPassword(userDTO.getPassword());
            userRepositary.save(user);

        }


    }

    @Override
    public List<User> gettingUser() {
        return userRepositary.findAll();
    }

    @Override
    public void deleteUser(int id) {
        userRepositary.deleteById(id);
    }
}
