package com.example.libarymanagement.service.impl;

import com.example.libarymanagement.model.Reader;
import com.example.libarymanagement.repositary.ReaderRepositary;
import com.example.libarymanagement.service.ReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReaderImpl implements ReaderService {
    @Autowired
    private ReaderRepositary readerRepositary;


    @Override
    public Reader saveReader(Reader reader) {
        return readerRepositary.save(reader);
    }

    @Override
    public void updateReader(int id, Reader reader) {
        Optional<Reader> optionalReader = readerRepositary.findById(id);
        if (optionalReader.isPresent()) {
            Reader readerObj = optionalReader.get();
            readerObj.setEmail(reader.getEmail());
            readerObj.setPhone(reader.getPhone());
            readerObj.setFirstName(reader.getFirstName());
            readerObj.setLastName(reader.getLastName());
            readerObj.setAddress(reader.getAddress());
            readerObj.setStaff(reader.getStaff());
            readerObj.setBook(reader.getBook());
            readerRepositary.save(readerObj);

        }
    }

    @Override
    public void deleteReader(int id) {
        readerRepositary.deleteById(id);
    }

    @Override
    public List<Reader> readerList() {
        return readerRepositary.findAll();
    }

    @Override
    public Page<Reader> getAllReaders(int pageNumber, int pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy));
        return readerRepositary.findAll(pageable);
    }
}
