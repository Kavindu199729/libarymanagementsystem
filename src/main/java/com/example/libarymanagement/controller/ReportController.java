package com.example.libarymanagement.controller;

import com.example.libarymanagement.dto.ReportDTO;
import com.example.libarymanagement.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/report")
public class ReportController {
    @Autowired
    private ReportService reportService;

    @PostMapping("/save-report")
    public String insert(@RequestBody ReportDTO reportDTO) {
        reportService.saveReport(reportDTO);

        return "inserted";
    }

    @DeleteMapping("/delete-report")
    public String delete(@RequestParam int id) {
        reportService.reportDelete(id);
        return "deleted";
    }
}
