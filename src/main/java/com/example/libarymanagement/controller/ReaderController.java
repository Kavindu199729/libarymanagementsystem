package com.example.libarymanagement.controller;

import com.example.libarymanagement.model.Reader;
import com.example.libarymanagement.service.ReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/reader")
public class ReaderController {
    @Autowired
    private ReaderService readerService;

    @PostMapping("/save")
    public String insert(@RequestBody Reader reader) {
        readerService.saveReader(reader);
        return "inserted";
    }

    @PutMapping("/updated/{id}")
    public String updated(@PathVariable int id, @RequestBody Reader reader) {
        readerService.updateReader(id, reader);
        return "updated";
    }

    @GetMapping("/all-readers")
    public List<Reader> getallReaders() {
        return readerService.readerList();
    }

    public String deleteReader(@PathVariable int id) {
        readerService.deleteReader(id);
        return "deleted";
    }

    @GetMapping("/readers-page")
    public ResponseEntity<Page<Reader>> getAllProducts(//pageable
                                                       @RequestParam(defaultValue = "0") int pageNumber,
                                                       @RequestParam(defaultValue = "10") int pageSize,
                                                       @RequestParam(defaultValue = "name") String sortBy) {
        Page<Reader> products = readerService.getAllReaders(pageNumber, pageSize, sortBy);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}
