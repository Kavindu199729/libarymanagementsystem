package com.example.libarymanagement.controller;

import com.example.libarymanagement.dto.UserDTO;
import com.example.libarymanagement.model.User;
import com.example.libarymanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/insert")
    public String userInsert(@RequestBody UserDTO userDTO) {
        userService.saveUser(userDTO);
        return "inserted";
    }

    @PutMapping("/update/{id}")
    public String userUpdate(@PathVariable int id, @RequestBody UserDTO userDTO) {
        userService.updateUser(id, userDTO);
        return "updated";

    }

    @GetMapping("/getall")
    public List<User> getall() {
        return userService.gettingUser();
    }
    @DeleteMapping("/delete-user/{id}")
    public String deleteUser(@PathVariable int id){
        userService.deleteUser(id);
        return "deleted";
    }

}
